# ------------------------------------------------------------------------------
# Build validator with Cargo
# ------------------------------------------------------------------------------

FROM rust:1-bullseye as cargo-build

WORKDIR /usr/src/cserph

COPY . .

RUN cargo build --release --target=x86_64-unknown-linux-gnu

# ------------------------------------------------------------------------------
# Create final image
# ------------------------------------------------------------------------------

FROM debian:bullseye-slim

RUN groupadd -g 1000 -r cserph \
    && useradd --no-log-init -r -u 1000 -g cserph cserph \
    && apt-get update && apt-get install -y \
        ca-certificates \
        openssl \
    && rm -rf /var/lib/apt/lists/*

COPY --from=cargo-build /usr/src/cserph/target/x86_64-unknown-linux-gnu/release/cserph /usr/local/bin/cserph

USER cserph

WORKDIR /mnt

CMD ["cserph"]
