use std::error::Error;
use std::fs;
use std::path::PathBuf;

use jsonschema::JSONSchema;
use log::debug;
use serde::{Deserialize, Serialize};
use serde_json::{from_str, to_value, Value};
use url::Url;

use crate::error;

pub trait CSMValidator {
    fn validate(&self, csm: &Value) -> Result<CSMSockets, Box<dyn Error>>;
    fn normalize(&self, csm: &Value) -> Result<Value, Box<dyn Error>>;
}

struct CSM020Validator {
    working_dir: PathBuf,
    csm_schema: JSONSchema,
    avro_schema: JSONSchema
}

impl CSM020Validator {
    fn check_sockets(&self, sockets: &Vec<CSM020Socket>) -> Result<(), Vec<Box<dyn Error>>> {
        let base_url = Url::from_directory_path(self.working_dir.clone()).expect("Could not create base url from working dir");

        let mut errors: Vec<Box<dyn Error>> = Vec::new();
        for socket in sockets {
            self.validate_socket_schema(&base_url, &mut errors, &socket.key_schema);
            self.validate_socket_schema(&base_url, &mut errors, &socket.value_schema);
        }
        if !errors.is_empty() {
            Err(errors)
        } else {
            Ok(())
        }
    }

    fn validate_socket_schema(&self, base_url: &Url, errors: &mut Vec<Box<dyn Error>>, socket_schema: &CSM020SocketSchema) {
        match socket_schema {
            CSM020SocketSchema::Avro { uri: url } => self.check_socket_schema(&base_url, errors, &url),
            // inline schemas have already been checked by the JSON schema so we don't need to do any additional validation
            CSM020SocketSchema::AvroInline { .. } => {}
        }
    }

    fn check_socket_schema(&self, base_url: &Url, errors: &mut Vec<Box<dyn Error>>, uri: &str) {
        debug!("Checking socket schema uri: {}", uri);

        match CSM020Validator::resolve_schema_uri(base_url, uri) {
            Ok(avro_schema) => {
                match from_str(&avro_schema) {
                    Ok(avro_schema) => {
                        if let Err(errs) =  self.avro_schema.validate(&avro_schema) {
                            for error in errs {
                                errors.push(error::ValidationError::new(format!("Validation error: {}", error)).into());
                            }
                            let message = format!("Socket schema uri \"{}\" points to invalid Avro schema", uri);
                            errors.push(error::ValidationError::new(message).into());
                        }
                    },
                    Err(e) => {
                        let message = format!("Socket schema uri \"{}\" points to invalid JSON: {}", uri, e);
                        errors.push(error::ValidationError::new(message).into());
                    }
                }
            },
            Err(e) => {
                errors.push(e);
            }
        }
    }

    fn normalize_sockets(base_url: &Url, sockets: &mut Vec<CSM020Socket>) -> Result<(), Box<dyn Error>> {
        for mut socket in sockets {
            if let CSM020SocketSchema::Avro { uri } = &socket.key_schema {
                socket.key_schema = CSM020SocketSchema::AvroInline {
                    schema: from_str(&CSM020Validator::resolve_schema_uri(&base_url, &uri)?)?
                };
            }
            if let CSM020SocketSchema::Avro { uri } = &socket.value_schema {
                socket.value_schema = CSM020SocketSchema::AvroInline {
                    schema: from_str(&CSM020Validator::resolve_schema_uri(&base_url, &uri)?)?
                };
            }
        }
        Ok(())
    }

    fn resolve_schema_uri(base_url: &Url, uri: &str) ->  Result<String, Box<dyn Error>> {
        let schema_url = base_url.join(&uri)?;

        match schema_url.scheme() {
            "file" => {
                match schema_url.to_file_path() {
                    Ok(path) => {
                        if !path.is_file() {
                            let message = format!("Local socket schema file does not exist: {}", path.display());
                            Err(error::ValidationError::new(message).into())
                        } else {
                            match fs::read_to_string(path.clone()) {
                                Ok(content) => Ok(content),
                                Err(e) => {
                                    let message = format!("Could not read local file \"{}\": {}", path.display(), e);
                                    Err(error::ValidationError::new(message).into())
                                }
                            }
                        }
                    },
                    Err(_) => {
                        let message = format!("Invalid socket schema url: {}", schema_url);
                        Err(error::ValidationError::new(message).into())
                    }
                }
            },
            "http" | "https" => {
                match reqwest::blocking::get(schema_url.clone()) {
                    Ok(resp) => if resp.status().is_success() {
                        match resp.text() {
                            Ok(text) => Ok(text),
                            Err(e) => {
                                let message = format!("Could not retrieve content for socket schema url \"{}\": {}", schema_url, e);
                                Err(error::ValidationError::new(message).into())
                            }
                        }
                    } else {
                        let message = format!("Non success status code while retrieving socket schema url \"{}\": {}", schema_url, resp.status());
                        Err(error::ValidationError::new(message).into())
                    },
                    Err(e) => {
                        let message = format!("Could not retrieve socket schema url \"{}\": {}", schema_url, e);
                        Err(error::ValidationError::new(message).into())
                    }
                }
            },
            _ => {
                let message = format!("Unsupported scheme for socket schema url: {}", schema_url);
                Err(error::ValidationError::new(message).into())
            }
        }
    }
}

impl CSMValidator for CSM020Validator {
    fn validate(&self, csm: &Value) -> Result<CSMSockets, Box<dyn Error>> {

        if let Err(errors) =  self.csm_schema.validate(&csm) {
            for error in errors {
                error!("Validation error: {}", error);
            }
            return Err(error::ValidationError::new("Invalid CSM format".to_string()).into());
        }

        let parsed: CSM020 = serde_json::from_value(csm.clone())?;

        debug!("Deserialized {:?}", parsed);

        let mut errors: Vec<Box<dyn Error>> = Vec::new();

        if let Err(errs) = self.check_sockets(&parsed.sockets.input) {
            for err in errs {
                errors.push(err);
            }
        }

        if let Err(errs) = self.check_sockets(&parsed.sockets.output) {
            for err in errs {
                errors.push(err);
            }
        }

        if !errors.is_empty() {
            for error in errors {
                error!("{}", error);
            }
            Err(error::ValidationError::new("Invalid socket(s) in CSM".to_string()).into())
        } else {
            Ok(parsed.into())
        }
    }

    fn normalize(&self, csm: &Value) -> Result<Value, Box<dyn Error>> {
        let base_url = Url::from_directory_path(self.working_dir.clone()).expect("Could not create base url from working dir");
        let mut parsed: CSM020 = serde_json::from_value(csm.clone())?;
        CSM020Validator::normalize_sockets(&base_url, &mut parsed.sockets.input)?;
        CSM020Validator::normalize_sockets(&base_url, &mut parsed.sockets.output)?;
        Ok(to_value(parsed)?)
    }
}

#[derive(Debug)]
pub struct CSMSockets {
    pub input: Vec<String>,
    pub output: Vec<String>
}

impl From<CSM020> for CSMSockets {
    fn from(csm: CSM020) -> Self {
        let sockets = csm.sockets;
        CSMSockets {
            input: sockets.input.iter().map(|s| s.name.clone()).collect(),
            output: sockets.output.iter().map(|s| s.name.clone()).collect()
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct CSM020 {
    sockets: CSM020Sockets
}

#[derive(Serialize, Deserialize, Debug)]
struct CSM020Sockets {
    input: Vec<CSM020Socket>,
    output: Vec<CSM020Socket>
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct CSM020Socket {
    name: String,
    key_schema: CSM020SocketSchema,
    value_schema: CSM020SocketSchema
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "type")]
enum CSM020SocketSchema {
    #[serde(rename = "avro")]
    Avro { uri: String },
    #[serde(rename = "avro-inline")]
    AvroInline { schema: Value }
}

pub fn from_value(csm_value: &Value, working_dir: PathBuf) -> Result<Box<dyn CSMValidator>, Box<dyn Error>> {
    let schema_url = match csm_value.as_object() {
        Some(obj) => match obj.get("$schema") {
            Some(schema) => match schema.as_str() {
                Some(schema_url) => schema_url,
                None => return Err(error::ValidationError::new("\"$schema\" property is not a string".to_string()).into())
            },
            None => return Err(error::ValidationError::new("CSM has no \"$schema\" property".to_string()).into())
        },
        None => return Err(error::ValidationError::new("CSM is not an object".to_string()).into())
    };

    match schema_url {
        "https://gitlab.com/centros.one/composer/schemas/-/raw/0.2.0/csm-schema.json" => {
            let csm_schema = include_str!("../schemas/composer/0.2.0/csm-schema.json");
            let avro_schema = include_str!("../schemas/composer/0.2.0/avro-spec.json");
            let csm_json: Value = from_str(csm_schema).expect("Could not read CSM schema");
            let avro_json: Value = from_str(avro_schema).expect("Could not read Avro schema");
            let csm_boxed: &'static Value = Box::leak(Box::new(csm_json));
            let avro_boxed: &'static Value = Box::leak(Box::new(avro_json.clone()));
            let csm_schema = JSONSchema::options()
                .with_document("json-schema:///avro-spec.json".to_string(), avro_json)
                .compile(csm_boxed)
                .expect("Could not compile CSM schema");
            let avro_schema = JSONSchema::compile(avro_boxed)
                .expect("Could not compile Avro schema");
            Ok(Box::new(CSM020Validator {
                working_dir,
                csm_schema,
                avro_schema
            }))
        },
        _ => Err(error::ValidationError::new(format!("Invalid value for \"$schema\" property: \"{}\"", schema_url)).into())
    }
}