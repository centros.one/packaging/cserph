use std::{env, fs, io, process};
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, stdout, Write};
use std::path::PathBuf;
use std::str::FromStr;

use clap::{Args, Parser, Subcommand, CommandFactory};
use clap_complete::{generate, Shell};
use env_logger::Builder;
use log::{debug, error, info, LevelFilter};
use serde_json::Value as JsonValue;
use serde_yaml::{from_reader, from_str, from_value, to_writer, Value};

mod error;
mod csm_validator;
mod porter_validator;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
#[clap(propagate_version = true)]
#[clap(arg_required_else_help(true))]
#[clap(subcommand_required(true))]
struct Cli {
    /// Log level to use
    #[clap(short, long, default_value = "info", possible_values = LOG_LEVELS, global = true)]
    log_level: String,

    /// The directory to look for CSM files in (default: current dir)
    #[clap(short, long)]
    directory: Option<String>,

    /// If set Porter files without CSM are seen as valid
    #[clap(short, long)]
    no_csm: bool,

    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Validates required service structure of a given directory
    Validate,
    /// Renders a Porter file template
    Render(RenderArgs),
    /// Generate autocomplete scripts for the given shell
    Autocomplete(AutocompleteArgs)
}


#[derive(Args)]
pub struct RenderArgs {
    /// The path the rendered Porter file should be written to (use - for stdout)
    #[clap(short, long)]
    output: Option<String>,
    /// Update a value in the resulting Porter file (format: JSON_POINTER=YAML_VALUE)
    ///
    /// Specify a single or multiple update values that that should be set in the resulting Porter
    /// file. The base format for specifying the update is JSON_POINTER=YAML_VALUE.
    ///
    /// Multiple values can either be specified by repeating the -u argument (-u ... -u ...) or
    /// combining them with comma as a delimiter (-u JSON_POINTER1=YAML_VALUE1,JSON_POINTER2=YAML_VALUE2,...).
    ///
    /// Note: The value to be updated must exist in the resulting Porter file already otherwise an
    /// error will be thrown.
    #[clap(short = 'u', long = "update", multiple_occurrences = true, use_value_delimiter = true)]
    set_values: Vec<String>,
    /// Disable normalization of Avro schemas in CSM
    #[clap(long)]
    no_normalize: bool
}

#[derive(Args)]
struct AutocompleteArgs {
    /// The shell to generate the autocomplete script for
    #[clap(arg_enum)]
    shell: Shell
}

const LOG_LEVELS: &[&str] = &["trace", "debug", "info", "warn", "error"];

fn main() {
    let cli = Cli::parse();

    let log_level = LevelFilter::from_str(&cli.log_level).unwrap();

    if let Err(err) = Builder::new().filter_level(log_level).try_init() {
        panic!("Could not initialize logger: {}", err);
    }

    let working_dir = match &cli.directory {
        Some(val) => PathBuf::from(val),
        None => match env::current_dir() {
            Ok(dir) => dir,
            Err(err) =>  {
                error!("Could not open cwd: {}", err);
                process::exit(255);
            }
        }
    };

    let no_csm = &cli.no_csm;

    let result = match &cli.command {
        Commands::Validate => perform_validation(working_dir, no_csm),
        Commands::Render(render_args) => perform_render(working_dir, no_csm, render_args),
        Commands::Autocomplete(args) => {
            let mut cmd = Cli::command();
            let bin_name = cmd
                .get_bin_name()
                .unwrap_or_else(|| cmd.get_name())
                .to_owned();
            generate(args.shell, &mut cmd, bin_name, &mut io::stdout());
            process::exit(0)
        }
    };

    process::exit(match result {
        Ok(_) => {
            info!("There were no errors");
            0
        },
        Err(err) => {
            error!("There were errors: {}", err);
            1
        }
    });
}


fn perform_validation(working_dir: PathBuf, no_csm: &bool) -> Result<(), Box<dyn Error>> {
    let working_dir = match fs::canonicalize(&working_dir) {
        Ok(dir) => dir,
        Err(err) => {
            error!("Could not read directory: {}", working_dir.display());
            return Err(err.into());
        }
    };

    info!("Reading directory: {}", working_dir.display());

    let porter_yaml = match find_porter_yaml(&working_dir)? {
        Some(path) => path,
        None => return Err(error::ValidationError::new("No porter.yaml.tmpl found".to_string()).into())
    };

    let file = File::open(&porter_yaml)?;
    let reader = BufReader::new(file);

    info!("Reading {}", porter_yaml.display());

    let yaml_value: Value = match from_reader(reader) {
        Ok(val) => val,
        Err(err) => {
            error!("Could not parse {}", porter_yaml.display());
            return Err(err.into());
        }
    };

    let porter_value: JsonValue = match from_value(yaml_value) {
        Ok(val) => val,
        Err(err) => {
            error!("Could not convert content of {} to internal json representation", porter_yaml.display());
            return Err(err.into());
        }
    };

    info!("Determining Porter version");
    let val = porter_validator::from_value(&porter_value)?;

    info!("Validating Porter file");
    val.validate(&porter_value)?;

    info!("Extracting CSM information");
    if let Some(csm_value) = val.get_csm(&porter_value)? {
        info!("Determining CSM schema");
        let csm_val = csm_validator::from_value(&csm_value, porter_yaml.parent().unwrap().to_path_buf())?;

        info!("Validating CSM information");
        let sockets = csm_val.validate(&csm_value)?;

        info!("Validating socket parameters");
        val.validate_socket_parameters(&porter_value, sockets)?;
    } else if !no_csm {
        return Err(error::ValidationError::new("No CSM information found".to_string()).into());
    }

    info!("Validating additional files");
    val.validate_additional_files(porter_yaml.parent().unwrap())?;

    Ok(())
}

fn perform_render(working_dir: PathBuf, no_csm: &bool, render_args: &RenderArgs) -> Result<(), Box<dyn Error>> {
    let working_dir = fs::canonicalize(&working_dir)?;
    let porter_yaml = match find_porter_yaml(&working_dir)? {
        Some(porter_yaml) => porter_yaml,
        None => return Err(error::ValidationError::new("No Porter file found".to_string()).into())
    };
    let file = File::open(&porter_yaml)?;
    let reader = BufReader::new(file);
    let mut porter_value: JsonValue = from_reader(reader)?;
    let val = porter_validator::from_value(&porter_value)?;
    let mut sockets = None;
    if let Some(mut csm_value) = val.get_csm(&porter_value)? {
        let csm_val = csm_validator::from_value(&csm_value, porter_yaml.parent().unwrap().to_path_buf())?;
        if !render_args.no_normalize {
            csm_value = csm_val.normalize(&csm_value)?;
        }
        sockets = Some(csm_val.validate(&csm_value)?);
        porter_value = val.replace_csm(&porter_value, &csm_value)?;
    } else if !no_csm {
        return Err(error::ValidationError::new("No CSM information found".to_string()).into());
    }
    for entry in &render_args.set_values {
        let split: Vec<&str> = entry.splitn(2, "=").collect();
        if split.len() != 2 {
            return Err(error::ValidationError::new(format!("Invalid -u option: {}", entry)).into());
        }
        let pointer = split[0];
        let value: JsonValue = match from_str(split[1]) {
            Ok(value) => value,
            Err(e) => return Err(error::ValidationError::new(format!("Could not parse value -u option {}: {}", entry, e)).into())
        };
        porter_value = val.set_value(&porter_value, pointer, value)?
    }
    val.validate(&porter_value)?;
    if let Some(sockets) = sockets {
        val.validate_socket_parameters(&porter_value, sockets)?;
    }
    let output: Box<dyn Write>= match &render_args.output {
        Some(val) => match val.as_str() {
            "-" => {
                info!("Writing output to stdout");
                Box::new(stdout())
            },
            _ => {
                info!("Writing output to: {}", val);
                Box::new(File::create(val)?)
            }
        },
        None => {
            let mut target_path = porter_yaml.parent().unwrap().to_path_buf();
            target_path.push("porter.yaml");
            info!("Writing output to: {}", target_path.display());
            Box::new(File::create(target_path)?)
        }
    };
    to_writer(output, &porter_value)?;
    Ok(())
}

fn find_porter_yaml(working_dir: &PathBuf) -> Result<Option<PathBuf>, std::io::Error> {
    let mut cnab_path = working_dir.clone();
    cnab_path.push("cnab");
    info!("Checking for Porter file in: {}", cnab_path.display());
    for entry in fs::read_dir(cnab_path)? {
        let entry = entry?;
        let filename = entry.file_name();
        let filename_string = filename.to_string_lossy();

        debug!("Checking filename: {}", filename_string);

        if filename_string == "porter.yaml.tmpl" {
            return Ok(Some(entry.path()))
        } else {
            debug!("Invalid filename, skipping")
        }
    }
    return Ok(None);
}
