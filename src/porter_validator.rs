use std::collections::BTreeMap;
use std::error::Error;
use std::path::Path;

use jsonschema::JSONSchema;
use serde::{Deserialize, Serialize};
use serde_json::{from_str, Map, to_value, Value};

#[cfg(target_os = "windows")]
use log::warn;
#[cfg(unix)]
use std::fs;
#[cfg(unix)]
use is_executable::IsExecutable;

use crate::csm_validator::CSMSockets;
use crate::error;

pub trait PorterValidator {
    fn validate(&self, porter_yaml: &Value) -> Result<(), Box<dyn Error>>;
    fn validate_socket_parameters(&self, porter_yaml: &Value, sockets: CSMSockets) -> Result<(), Box<dyn Error>>;
    fn validate_additional_files(&self, bundle_dir: &Path) -> Result<(), Box<dyn Error>>;
    fn get_csm(&self, porter_yaml: &Value) -> Result<Option<Value>, Box<dyn Error>>;
    fn replace_csm(&self, porter_yaml: &Value, new_csm: &Value) -> Result<Value, Box<dyn Error>>;
    fn set_value(&self, porter_yaml: &Value, pointer: &str, value: Value) -> Result<Value, Box<dyn Error>>;
}

fn get_socket_parameter_name(direction: &str, socket_name: &String) -> String {
    format!("sockets-{}-{}-topic", direction, socket_name)
}

struct Porter033Validator {
    schema: JSONSchema
}

impl PorterValidator for Porter033Validator {
    fn validate(&self, porter_yaml: &Value) -> Result<(), Box<dyn Error>> {
        let result = self.schema.validate(&porter_yaml);
        if let Err(errors) = result {
            for error in errors {
                error!("Validation error: {}", error);
            }
            return Err(error::ValidationError::new("Invalid Porter file".to_string()).into());
        }
        Ok(())
    }

    fn validate_socket_parameters(&self, porter_yaml: &Value, sockets: CSMSockets) -> Result<(), Box<dyn Error>> {
        let mut valid = true;
        if !sockets.input.is_empty() || sockets.output.is_empty() {
            let porter_value: Porter033Yaml = serde_json::from_value(porter_yaml.clone())?;
            let mut socket_params = BTreeMap::new();
            socket_params.extend(sockets.input.iter().map(|s| (get_socket_parameter_name("input", s), ("input", s))));
            socket_params.extend(sockets.output.iter().map(|s| (get_socket_parameter_name("output", s), ("output", s))));
            for parameter in porter_value.parameters {
                if let Some((direction, socket_name)) = socket_params.remove(&parameter.name) {
                    if parameter.type_ != "string" {
                        error!("Parameter error: Invalid type \"{}\" for parameter \"{}\" for {} socket \"{}\", should be \"string\"", parameter.type_, parameter.name, direction, socket_name);
                        valid = false;
                    }
                }
            }
            if !socket_params.is_empty() {
                for (param_name, (direction, socket_name)) in socket_params {
                    error!("Parameter error: Missing parameter \"{}\" for {} socket \"{}\"", param_name, direction, socket_name)
                }
                valid = false;
            }
        }
        match valid {
            true => Ok(()),
            false => Err(error::ValidationError::new("Invalid or missing socket parameter(s)".to_string()).into())
        }
    }

    #[cfg(unix)]
    fn validate_additional_files(&self, bundle_dir: &Path) -> Result<(), Box<dyn Error>> {
        let mut invalid = false;
        for entry in fs::read_dir(bundle_dir)? {
            let entry = entry?;
            let filename = entry.file_name();
            let filename_string = filename.to_string_lossy();

            if filename_string.ends_with(".sh") && !entry.path().is_executable() {
                error!("File error: Script \"{}\" is not executable", filename_string);
                invalid = true;
            }
        }
        match invalid {
            true => Err(error::ValidationError::new("Invalid files found".to_string()).into()),
            false => Ok(())
        }
    }

    #[cfg(target_os = "windows")]
    fn validate_additional_files(&self, _: &Path) -> Result<(), Box<dyn Error>> {
        warn!("File validation is not available in Windows");
        Ok(())
    }

    fn get_csm(&self, porter_yaml: &Value) -> Result<Option<Value>, Box<dyn Error>> {
        let porter_value: Porter033Yaml = serde_json::from_value(porter_yaml.clone())?;
        if let Some(custom) = porter_value.custom {
            if custom.contains_key("one.centros.csm") {
                let value = custom.get("one.centros.csm").unwrap();
                return Ok(Some(value.clone()));
            }
        }
        Ok(None)
    }

    fn replace_csm(&self, porter_yaml: &Value, new_csm: &Value) -> Result<Value, Box<dyn Error>> {
        let mut porter_value: Porter033Yaml = serde_json::from_value(porter_yaml.clone())?;
        let mut custom: Map<String, Value> = match porter_value.custom {
            Some(ref custom) => custom.clone(),
            None => Map::new()
        };
        custom.insert("one.centros.csm".to_string(), new_csm.clone());
        porter_value.custom = Some(custom);
        Ok(to_value(porter_value)?)
    }

    fn set_value(&self, porter_yaml: &Value, pointer: &str, value: Value) -> Result<Value, Box<dyn Error>> {
        let mut result = porter_yaml.clone();
        match result.pointer_mut(pointer) {
            Some(key) => *key = value.into(),
            None => return Err(error::ValidationError::new(format!("Value pointed to by \"{}\" does not exist", pointer)).into())
        }
        Ok(result)
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct Porter033Yaml {
    custom: Option<Map<String, Value>>,
    parameters: Vec<Porter033Parameter>,
    #[serde(flatten)]
    extra: Map<String, Value>
}

#[derive(Serialize, Deserialize, Debug)]
struct Porter033Parameter {
    name: String,
    #[serde(rename = "type")]
    type_: String,
    #[serde(flatten)]
    extra: Map<String, Value>
}

pub fn from_value(_porter_yaml: &Value) -> Result<Box<dyn PorterValidator>, Box<dyn Error>> {
    // for now we only support Porter 0.33
    let porter_schema = include_str!("../schemas/porter/0.33/porter-schema.json");
    let porter_json: Value = from_str(porter_schema).unwrap();
    let schema_boxed: &'static Value = Box::leak(Box::new(porter_json));
    let porter_validator = JSONSchema::compile(&schema_boxed).unwrap();
    Ok(Box::new(Porter033Validator {
        schema: porter_validator
    }))
}