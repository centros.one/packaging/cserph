use std::{error, fmt};

#[derive(Debug, Clone)]
pub struct ValidationError {
    message: String
}

impl ValidationError {
    // A public constructor method
    pub fn new(message: String) -> ValidationError {
        ValidationError {
            message
        }
    }
}

impl fmt::Display for ValidationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl error::Error for ValidationError{}